/**
 Template Controllers

 @module Templates
 */

/**
 The view3contract template

 @class [template] views_view3_contract
 @constructor
 */

Template['views_view3_contract'].onCreated(function() {
    Meta.setSuffix(TAPi18n.__("dapp.view3contract.title"));
});