/**
Template Controllers

@module Templates
*/

/**
The multiply contract template.

Note, the MultiplyContract object is now housed in client/lib/contracts/MultiplyContract.sol

@class [template] components_rentalContract
@constructor
*/


// solidity source code
var source1 = "" + 
"contract test {\n" +
"   function multiply(uint a) returns(uint d) {\n" +
"       return a * 7;\n" +
"   }\n" +
"}\n";

var source = "" +
"contract RentalContract {\n" +
"   struct PaidRent {\n" +
"       uint id;\n" +
"       uint value;\n" +
"   }\n" +
"   PaidRent[] public paidrents;\n" +
"   uint public createdTimestamp;\n" +
"   uint public rent;\n" +
"   string public house;\n" +
"   address public landlord;\n" +
"   address public tenant;\n" +
"   enum State { Created, Concession, Terminated}\n" +
"   State public state;\n" +
"   function RentalContract(uint _rent, string _house){\n" +
"       rent = _rent;\n" +
"       house = _house;\n" +
"       landlord = msg.sender\n" +
"       createdTimestamp = block.timestamp\n" +
"   }\n" +
"   modifier require(bool _condition) {\n" +
"       if (!_condition) throw;\n" +
"       _\n" +
"   }\n" +
"   modifier onlyLandlord() {\n" +
"       if (msg.sender != landlord) throw;\n" +
"       _\n" +
"   }\n" +
"   modifier onlyTenant() {\n" +
"       if (msg.sender != tenant) throw;\n" +
"       _\n" +
"   }\n" +
"   modifier inState(State _state) {\n" +
"       if (state != _state) throw;\n" +
"       _\n" +
"   }\n" +
"   function getPaidRents() internal returns(PaidRent[]) {\n" +
"       return paidrents;\n" +
"   }\n" +
"   function getHouse() constant returns(string) {\n" +
"       return house;\n" +
"   }\n" +
"   function getLandlord() constant returns(address) {\n" +
"       return landlord;\n" +
"   }\n" +
"   function getTenant() constant returns(address) {\n" +
"       return tenant;\n" +
"   }\n" +
"   function getRent() constant returns(uint) {\n" +
"       return rent;\n" +
"   }\n" +
"   function getContractCreated() constant returns(uint) {\n" +
"       return createdTimestamp;\n" +
"   }\n" +
"   function getContractAddress() constant returns(address) {\n" +
"       return this;\n" +
"   }\n" +
"   function getState() returns(State) {\n" +
"       return state;\n" +
"   }\n" +
"   event agreementConfirmed();\n" +
"   event paidRent();\n" +
"   event contractTerminated();\n" +
"   function confirmAgreement()\n" +
"       inState(State.Created)\n" +
"       require(msg.sender != landlord)\n" +
"   {\n" +
"       agreementConfirmed();\n" +
"       tenant = msg.sender;\n" +
"       state = State.Concession;\n" +
"   }\n" +
"   function payRent()\n" +
"       onlyTenant\n" +
"       inState(State.Concession)\n" +
"       require(msg.value == rent)\n" +
"   {\n" +
"       paidRent();\n" +
"       landlord.send(msg.value);\n" +
"       paidrents.push(PaidRent({\n" +
"           id: paidrents.length + 1,\n" +
"           value: msg.value\n" +
"       }));\n" +
"   }\n" +
"   function terminateContract()\n" +
"       onlyLandlord\n" +
"   {\n" +
"       contractTerminated();\n" +
"       landlord.send(this.balance);\n" +
"       state = State.Terminated;\n" +
"   }\n" +
"}\n";

// Construct Multiply Contract Object and contract instance
var contractInstance;

// When the template is rendered
Template['components_rentalContract'].onRendered(function(){
    TemplateVar.set('state', {isInactive: true});
});

Template['components_rentalContract'].helpers({

	/**
	Get multiply contract source code.
	
	@method (source)
	*/

	'source': function(){
		return source;
	},
});

Template['components_rentalContract'].events({

	/**
	On "Create New Contract" click
	
	@event (click .btn-default)
	*/

	"click .btn-default": function(event, template){ // Create Contract
        var rent = template.find("#rent").value;
        var house = template.find("#house").value;

        TemplateVar.set('state', {isMining: true});
        
        // Set coinbase as the default account
        web3.eth.defaultAccount = web3.eth.coinbase;
        
        // assemble the tx object w/ default gas value
        var transactionObject = {
            data: RentalContract.bytecode, 
            gasPrice: web3.eth.gasPrice,
            gas: 500000,
            from: web3.eth.coinbase//web3.eth.accounts[0]
        };
        
        // estimate gas cost then transact new MultiplyContract
        web3.eth.estimateGas(transactionObject, function(err, estimateGas){
            // multiply by 10 hack for testing
            if(!err)
                transactionObject.gas = estimateGas * 10;

            RentalContract.new(rent, house, transactionObject,
                                 function(err, contract){
                if(err)
                    return TemplateVar.set(template, 'state', {isError: true, error: String(err)});
                
                if(contract.address) {
                    TemplateVar.set(template, 'state', {isMined: true, address: contract.address, source: source});
                    contractInstance = contract;
                    contractInstance.getRent.call(function(err, result) {
                        console.log(result);
                    });
                    contractInstance.getHouse.call(function(err, result) {
                        console.log(result);
                    });
                }
            });
        });
	},

    
	/**
	On Multiply Number Input keyup
	
	@event (keyup #multiplyValue)
	*/

	"keyup #multiplyValue": function(event, template){
        // the input value
		var value = template.find("#multiplyValue").value;  
        
        // call MultiplyContract method `multiply` which should multiply the `value` by 7
		contractInstance.multiply.call(value, function(err, result){
            TemplateVar.set(template, 'rentalResult'
                            , result.toString());
            
            if(err)
                TemplateVar.set(template, 'rentalResult'
                                , String(err));
        });
	},
});
