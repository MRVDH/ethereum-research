/**
Template Controllers

@module Templates
*/

/**
The multiply contract template.

Note, the MultiplyContract object is now housed in client/lib/contracts/MultiplyContract.sol

@class [template] components_rentalContractInfo
@constructor
*/

// Construct Multiply Contract Object and contract instance
var contractInstance;

function updateTenant(template) {
    contractInstance.getTenant.call(function(err, result) {
        TemplateVar.set(template, 'tenant', result);
    });
}

function updateState(template) {
    contractInstance.getState.call(function(err, result) {
        var state;
        switch (result.c[0]) {
            case 0:
                state = "Created";
                TemplateVar.set(template, 'state', {isCreated: true});
                break;
            case 1:
                state = "Concession";
                TemplateVar.set(template, 'state', {isConcession: true});
                break;
            case 2:
                state = "Terminated";
                TemplateVar.set(template, 'state', {isTerminated: true});
                break;
        }
        TemplateVar.set(template, 'contractState', state);
    });
}

// When the template is rendered
Template['components_rentalContractInfo'].onRendered(function(){

    contractInstance = RentalContract.at(Router.current().params.address);

    var template = this;

    contractInstance.getRent.call(function(err, result) {
        TemplateVar.set(template, 'rent', result.c[0]);
    });
    contractInstance.getHouse.call(function(err, result) {
        TemplateVar.set(template, 'house', result);
    });
    contractInstance.getLandlord.call(function(err, result) {
        TemplateVar.set(template, 'landlord', result);
    });
    updateTenant(template);
    contractInstance.getContractCreated.call(function(err, result) {
        TemplateVar.set(template, 'contractCreated', (new Date(result * 1000)).toString());
    });
    updateState(template);
});

Template['components_rentalContractInfo'].helpers({

	/**
	Get multiply contract source code.
	
	@method (source)
	*/

	'contractAddress': function(){
		return Router.current().params.address;
	},
});

Template['components_rentalContractInfo'].events({

	/**
	On "Create New Contract" click
	
	@event (click .btn-default)
	*/

	"click .btn-confirm": function(event, template){ // confirm agreement
        contractInstance.confirmAgreement.sendTransaction({from:web3.eth.coinbase, gas:500000}, function(error, result) {
            TemplateVar.set(template, 'state', {isConfirming: true});
            //could do loader to indicate transaction is worked
            var event = contractInstance.agreementConfirmed();
            event.watch( function(err, res){
                //transaction is worked and agreementConfirmed event is fired.
                    if (!error) {
                        updateTenant(template);
                        updateState(template);
                    }
                }
            );
        });
	},

    
	/**
	On Multiply Number Input keyup
	
	@event (keyup #multiplyValue)
	*/

	"keyup #multiplyValue": function(event, template){
        // the input value
		var value = template.find("#multiplyValue").value;  
        
        // call MultiplyContract method `multiply` which should multiply the `value` by 7
		contractInstance.multiply.call(value, function(err, result){
            TemplateVar.set(template, 'rentalResult'
                            , result.toString());
            
            if(err)
                TemplateVar.set(template, 'rentalResult'
                                , String(err));
        });
	},
});
