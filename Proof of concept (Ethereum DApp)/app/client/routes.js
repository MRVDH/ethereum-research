/**
Template Controllers

@module Routes
*/

/**
The app routes

@class App routes
@constructor
*/

// Change the URLS to use #! instead of real paths
// Iron.Location.configure({useHashPaths: true});

// Router defaults
Router.configure({
    layoutTemplate: 'layout_main',
    notFoundTemplate: 'layout_notFound',
    yieldRegions: {
        'layout_header': {to: 'header'}
        , 'layout_footer': {to: 'footer'}
    }
});

// ROUTES

/**
The receive route, showing the wallet overview

@method dashboard
*/

// Default route
Router.route('/', {
    template: 'views_view1',
    name: 'home'
});

// Route for testnet
Router.route('/testnet', {
    template: 'views_view1',
    name: 'testnet'
});

// Route for contract create
Router.route('/contract', {
    template: 'views_view3',
    name: 'contract'
});

// Route for contract info
Router.route('/contract/:address', {
    template: 'views_view3_contract',
    name: 'contractinfo'
});